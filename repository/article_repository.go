package repository

import (
	"errors"

	"gitlab.com/fannyhasbi/article-service/model"
)

type ArticleStorage struct {
	ArticleMap map[string]model.Article
}

func CreateArticleStorage() *ArticleStorage {
	return &ArticleStorage{
		ArticleMap: make(map[string]model.Article),
	}
}

func (s *ArticleStorage) IsSlugExist(slug string) bool {
	for _, article := range s.ArticleMap {
		if article.Slug == slug {
			return true
		}
	}
	return false
}

func (s *ArticleStorage) SaveArticle(article *model.Article) string {
	s.ArticleMap[article.Slug] = *article
	return article.Slug
}

// FindByCategory return published articles filter by category
func (s *ArticleStorage) GetPublishedArticleByCategory(category model.Category) ([]model.Article, error) {
	var articles []model.Article
	for _, article := range s.ArticleMap {
		if article.Status == model.ArticlePublished && article.IsContainsCategory(category) {
			articles = append(articles, article)
		}
	}
	return articles, nil
}

func (s *ArticleStorage) GetPublishedArticleBySlug(slug string) (model.Article, error) {
	article, ok := s.ArticleMap[slug]
	if !ok {
		return model.Article{}, errors.New("Slug doesn't exist")
	}
	if article.Status != model.ArticlePublished {
		return model.Article{}, errors.New("Article isn't published yet")
	}
	return article, nil
}
