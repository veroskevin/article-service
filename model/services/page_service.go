package services

import (
	"errors"
	"fmt"

	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
)

// PageServiceInMemory for page service
type PageServiceInMemory struct {
	Repository *repository.InMemoryPageStorage
}

// NewPageService create new service for Page
func NewPageService(repo *repository.InMemoryPageStorage) model.PageService {
	return &PageServiceInMemory{
		Repository: repo,
	}
}

func (a *PageServiceInMemory) CheckSlugIsExist(slug string) bool {
	return a.Repository.IsSlugExist(slug)
}

// IsPageLimited check whether the page is more than the limit or not
func (ps *PageServiceInMemory) IsPageLimited() bool {
	return ps.Repository.IsPageLimited()
}

// Getting all Page from memory
func (ps *PageServiceInMemory) GetAllPages() []model.Page {
	return ps.Repository.GetAllPages()
}

// List all published page
func (ps *PageServiceInMemory) ListPublishedPages() (list []model.Page, err error) {
	pages := ps.Repository.GetAllPages()
	for _, page := range pages {
		if page.Status == model.PagePublished {
			list = append(list, page)
		}
	}

	if len(list) == 0 {
		err = errors.New("No published page found")
	}

	return
}

// Finding single page by Slug
func (ps *PageServiceInMemory) FindPageBySlug(slug string) (model.Page, error) {
	published_pages := ps.Repository.GetAllPages()
	for _, page := range published_pages {
		if page.Slug == slug && page.Status == model.PagePublished {
			return page, nil
		}
	}
	return model.Page{}, fmt.Errorf("Page not found!")
}
